# Version History

[TOC]: # " "

- [0.8.6](#086)
- [0.8.4](#084)
- [0.8.2](#082)
- [0.8.0](#080)


## 0.8.6

* Fix: update dependencies

## 0.8.4

* Add: `callback` argument to `boxedState.save(callback)` and passed to `saveState`
  `function(modified, boxed, callback)` to allow for callback when setting React state to state
  update completion callback.
* Add: implement `boxOptions(options)` to change box only options to be used for box creation

## 0.8.2

* Fix: exports

## 0.8.0

* Change: factor out the functionality from `boxed-immutable` module

